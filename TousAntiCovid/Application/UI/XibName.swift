// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  XibName.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/04/2020 - for the TousAntiCovid project.
//

import Foundation

enum XibName: String {
    
    // MARK:: - Onboarding cells -
    case onboardingImageCell = "OnboardingImageCell"
    case onboardingGovernmentCell = "OnboardingGovernmentCell"
    case onboardingWorkingStepCell = "OnboardingWorkingStepCell"
    case onboardingGestureCell = "OnboardingGestureCell"
    
    // MARK: - Other cells -
    case standardCell = "StandardCell"
    case textCell = "TextCell"
    case textFieldCell = "TextFieldCell"
    case titleCell = "TitleCell"
    case buttonCell = "ButtonCell"
    case switchCell = "SwitchCell"
    case appVersionCell = "AppVersionCell"
    case sickStateHeaderCell = "SickStateHeaderCell"
    case centeredImageTextCell = "CenteredImageTextCell"
    case callCell = "CallCell"
    case emptyCell = "EmptyCell"
    case audioCell = "AudioCell"
    case stateAnimationCell = "StateAnimationCell"
    case standardTextFieldCell = "StandardTextFieldCell"
    case infoCell = "InfoCell"
    case linkButtonCell = "LinkButtonCell"
    case activationButtonCell = "ActivationButtonCell"
    case lastInfoCell = "LastInfoCell"
    case sharingCell = "SharingCell"
    case keyFiguresCell = "KeyFiguresCell"
    case declareCell = "DeclareCell"
    case contactStatusCell = "ContactStatusCell"
    case menuCell = "MenuCell"
    case cardTextCell = "CardTextCell"
    case paragraphCell = "ParagraphCell"
    case imageCell = "ImageCell"
    case keyFigureCell = "KeyFigureCell"
    
}
